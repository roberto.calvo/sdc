# Ejemplos código Productores/Consumidores

## consumer_busy_waiting

Esta implementación realiza espera activa y además podría sufrir interbloqueos. Esta implementación arranca 1 thread para consumidor y 1 thread para el productor. NOTA: Esta implementación es erronea y nunca se debería usar. Se muestra como ejemplo de como NO implementar productores/consumidores.

## consumer_condvariable

Esta implementación utiliza mutex y variables condición para solucionar el problema de productores/consumidores. No realiza espera activa y no sufre de interbloqueos. Esta implementación arranca 1 thread para consumidor y 1 thread para el productor.

## consumer_confvariable_multi

Esta implementación utiliza mutex y variables condición para solucionar el problema de productores/consumidores. No realiza espera activa y no sufre de interbloqueos. Esta implementación arranca multiples threads de productores y 1 único thread de consumidor. Ejemplo práctico para ver la diferencia entre **pthread_cond_broadcast** y **pthread_cond_signal**

