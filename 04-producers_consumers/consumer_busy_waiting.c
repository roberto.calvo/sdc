/*
* Copyright (C) 2021 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

// Productor Consumidor solucionado con mutex y variables condición
// Asignatura: Sistemas Distribuidos y Concurrentes
// Universidad Rey Juan Carlos

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define MAX_BUFFER 10
#define MAX_DATA_PRODUCER 50
#define SLEEP_TIME 50000


pthread_mutex_t mutex;


int n_elems;
int buffer[MAX_BUFFER];


void producer (void) {

  int pos = 0 ;
  int aux_pos = 0;

  for (int i=0; i<MAX_DATA_PRODUCER; i++) {

    pthread_mutex_lock(&mutex);
    while (n_elems == MAX_BUFFER) {}

    buffer[pos] = i;
    aux_pos = pos;
    pos = (pos + 1) % MAX_BUFFER;
    n_elems ++;

    pthread_mutex_unlock(&mutex);
    printf("Producer[%d] %d \n", aux_pos, i);
    usleep(SLEEP_TIME);
    
  }
  pthread_exit(0);
}

void consumer (void) {

  int data = 0;
  int pos = 0;
  int aux_pos = 0;

  for (int i=0; i<MAX_DATA_PRODUCER; i++) {


    pthread_mutex_lock(&mutex);
    while (n_elems == 0) {}

    data = buffer[pos];
    aux_pos = pos;
    pos = (pos + 1) % MAX_BUFFER;
    n_elems--;

    pthread_mutex_unlock(&mutex);
    printf("Consumer[%d] %d \n", aux_pos, data);
    usleep(SLEEP_TIME*3);
  }
  pthread_exit(0);
}




int main(void)
{

  pthread_t th_consumer, th_producer;

  pthread_mutex_init(&mutex, NULL);

  pthread_create(&th_consumer, NULL, (void *)&producer, NULL);  
  pthread_create(&th_producer, NULL, (void *)&consumer, NULL);

  pthread_join(th_consumer, NULL);
  pthread_join(th_producer, NULL);

  pthread_mutex_destroy(&mutex);


  return 0;

}




