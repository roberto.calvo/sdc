/*
*
* Copyright (C) 2021 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>


#define TOTAL_THREADS 20
#define N 100
#define SECTONANO   1000000000
#define MILISTONANO 1000000

#define MIN_SLEEP_MS 50
#define MAX_SLEEP_MS 100

pthread_mutex_t mutex;
int value = 0;


void thread_function1 (void) {

    struct timespec begin, end, sleep_time;

    for (int i=0; i<N; i++) {

        clock_gettime(CLOCK_MONOTONIC, &begin);
        pthread_mutex_lock(&mutex);        
        clock_gettime(CLOCK_MONOTONIC, &end);        
        value = value + i;
        pthread_mutex_unlock(&mutex);

        int diff = (end.tv_sec - begin.tv_sec)*SECTONANO + end.tv_nsec - begin.tv_nsec;
        printf("%d\n", diff);

        // Sleep for a random time between MIN_SLEEP_MS and MAX_SLEEP_MS
        sleep_time.tv_sec = 0;
        sleep_time.tv_nsec = ((rand() % (MAX_SLEEP_MS - MIN_SLEEP_MS + 1)) + MIN_SLEEP_MS)*MILISTONANO;
        nanosleep(&sleep_time, NULL);

    }
}


int main (int argc, char* argv[]) {

    pthread_t threads[TOTAL_THREADS];
    
    pthread_mutex_init(&mutex, NULL);

    for (int i=0; i<TOTAL_THREADS; i++){        
        pthread_create(&threads[i],NULL,  (void *)&thread_function1, NULL);
    }

    for(int i=0; i<TOTAL_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    pthread_mutex_destroy(&mutex);


    return 0;
}