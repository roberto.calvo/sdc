/*
* Copyright (C)  by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

#include <stdio.h>
#include <string.h>
#include <pthread.h>


pthread_mutex_t mutex;
int value = 0, N=1000;

void thread_function1 (void) {

    for (int i=0; i<N; i++) {
        pthread_mutex_lock(&mutex);
        value = value + i;
        pthread_mutex_unlock(&mutex);
    }
}

void thread_function2 (void) {

    for (int i=0; i<N; i++) {
        pthread_mutex_lock(&mutex);
        value = value - i;
        pthread_mutex_unlock(&mutex);
    }
}

void thread_function3 (void) {

    for (int i=0; i<N; i++) {
        pthread_mutex_lock(&mutex);
        value = value * i;
        pthread_mutex_unlock(&mutex);
    }
}


int main (int argc, char* argv[]) {

    pthread_t th1, th2, th3;

    pthread_mutex_init(&mutex, NULL);

    pthread_create(&th1, NULL, (void *)&thread_function1, NULL);
    pthread_create(&th2, NULL, (void *)&thread_function2, NULL);

    pthread_join(th1, NULL);
    pthread_join(th2, NULL);

    pthread_mutex_destroy(&mutex);

    printf ("Resultado final = %d\n", value);


    return 0;
}

