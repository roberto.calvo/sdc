# SDC

Bienvenido al repositorio de código de la asignatura de Sistemas Distribuidos y Concurrentes del Grado de Robotica Software de la Universidad Rey Juan Carlos.
En este repositorio encontrarás todo el código de ayuda y ejemplos utilizados en clase.

## Ejemplos de Concurrencia

* 01-mutex
* 02-semaphore
* 03-monitores
* 04-producers_consumers
* 05-barrier


## Ejercicios Opcionales

* Ejercicio1: Implementa el ejercicio de productores/consumidores utilizando exclusivamente semaforos.
* Ejercicio2: Implementa el ejercicio de barreras, sin utilizar pthread_barrier.