/*
*
* Copyright (C) 2021 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define TOTAL_THREADS 100
#define MAX_THREADS 10

sem_t mutex;

void *count(void *ptr) {

    int id_thread = *(int*)ptr;
    printf("[W] Thread %d está esperando semaforo\n", id_thread);

    sem_wait(&mutex);

    printf("[R] Thread %d está ejecutando\n", id_thread);
    sleep(3);
    printf("[F] Thread %d ha finalizado\n", id_thread);

    sem_post(&mutex);

}
int main (int argc, char* argv[]) {

    sem_init(&mutex, 0, MAX_THREADS);

    pthread_t threads[TOTAL_THREADS];

    for (int i=0; i<TOTAL_THREADS; i++){
        int id = i;
        pthread_create(&threads[i],NULL, count, (void*) &id);
    }

    for(int i=0; i<TOTAL_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }


    return 0;
}