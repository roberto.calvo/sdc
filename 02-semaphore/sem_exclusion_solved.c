/*
*
* Copyright (C) 2021 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

#define NUM_THREADS 4
#define MAX_COUNT 100000

long counter=0;

sem_t mutex;


void *count(void *ptr) {

    long max = MAX_COUNT/NUM_THREADS;

    for (long i=0; i < max; i++) {
        sem_wait(&mutex);
        counter += 1;
        sem_post(&mutex);
    }

}
int main (int argc, char* argv[]) {

    pthread_t threads[NUM_THREADS];
    sem_init(&mutex, 0, 1);

    for (int i=0; i<NUM_THREADS; i++){
        pthread_create(&threads[i],NULL, count, NULL);
    }

    for(int i=0; i<NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    printf("Contador:%d\n", counter);
}