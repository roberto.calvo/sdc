/*
*
* Copyright (C) 2021 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

//  gcc  barrier_pthread.c -o barrier_pthread -lpthread


#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define TOTAL_THREADS 10


int counter = 0;
pthread_barrier_t   barrier;


void *count(void *ptr) {

    int id = *(int*)ptr;
    int sleep_time;
    struct timespec time;

    sleep_time = rand() % 10;
    printf("[%d] Durmiendo %d segundos\n", id, sleep_time);
    sleep(sleep_time);



    printf("[%i] Esperando a que lleguen el resto de threads \n", id);
    pthread_barrier_wait (&barrier);
    clock_gettime(CLOCK_REALTIME, &time);

    printf("[%d.%d] [%d] Thread ha finalizado\n", time.tv_sec, time.tv_nsec, id);

    pthread_exit(NULL);

}
int main (int argc, char* argv[]) {

    pthread_barrier_init (&barrier, NULL, TOTAL_THREADS);

    pthread_t threads[TOTAL_THREADS];

    for (int i=0; i<TOTAL_THREADS; i++){
        pthread_create(&threads[i],NULL, count, (void*) &i);
    }

    for(int i=0; i<TOTAL_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}