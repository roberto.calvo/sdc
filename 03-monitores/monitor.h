/*
* Copyright (C) 2020 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

// Productor Consumidor solucionado con mutex y variables condición
// Asignatura: Sistemas Distribuidos y Concurrentes
// Universidad Rey Juan Carlos



void init_module ();
void finish_module ();


// Las siguientes funciones son thread-safe
void move_robot_right();
void move_robot_left();
void move_robot_forward();
void move_robot_backward();

void print_commands();
