/*
* Copyright (C) 2020 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

// Productor Consumidor solucionado con mutex y variables condición
// Asignatura: Sistemas Distribuidos y Concurrentes
// Universidad Rey Juan Carlos

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <pthread.h>

#include "monitor.h"


#define MAX_BUFFER 10

pthread_mutex_t mutex;
pthread_cond_t is_full;

pthread_t thread_processing;

int buffer[MAX_BUFFER];
int buffer_counter=0;

void my_sleep() {

  struct timespec tm;
  tm.tv_sec = 0;
  tm.tv_nsec = rand() % 3;

  int res = clock_nanosleep(CLOCK_MONOTONIC, 0, &tm, NULL);
  if (res!=0)
    printf("Error al intentar hacer el sleep\n");

}

// Funcion que resta 2  timespec
void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result) {
  if ((stop->tv_nsec - start->tv_nsec) < 0) {
    result->tv_sec = stop->tv_sec - start->tv_sec - 1;
    result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
  } else {
    result->tv_sec = stop->tv_sec - start->tv_sec;
    result->tv_nsec = stop->tv_nsec - start->tv_nsec;
  }

  return;
}


// Esta función procesa las funciones del buffer en el Robot en lotes de MAX_BUFFER
// cada X segundos. Recuerda utilizar pthread_cond_broadcast() en lugar de
// pthread_cond_signal() para despertar a todos los threads esperando en la variable condición.

void process_commands () {

  const long PROCESS_TIME_NS = 10000000;
  struct timespec previous_t, current_t, diff_t, sleep_t;
  int exit_counter=0;

  sleep_t.tv_sec=0; sleep_t.tv_nsec=1000;

  clock_gettime(CLOCK_MONOTONIC, &current_t);
  clock_gettime(CLOCK_MONOTONIC, &previous_t);

  while(1){

    clock_gettime(CLOCK_MONOTONIC, &current_t);
    timespec_diff(&previous_t, &current_t, &diff_t);

    // Solo se procesan los comandos PROCESS_TIME_NS nanosegundos. 
    if (diff_t.tv_nsec > PROCESS_TIME_NS) {

      pthread_mutex_lock(&mutex);
      if (buffer_counter==0)
        exit_counter++;

      // Simulamos el procesamiento de comandos limpiando el buffer
      printf("Procesando %d comandos del robot\n", buffer_counter);
      buffer_counter=0;
      memset(&buffer, 0, sizeof(char)*MAX_BUFFER);

      // Como el buffer está vacio podemos ejecutar el cond_broadcast para mandar
      // la señal y despertar a todos los threads bloqueados en pthread_cond_wait
      pthread_cond_broadcast(&is_full);
      pthread_mutex_unlock(&mutex);

      memcpy(&previous_t,&current_t,sizeof(struct timespec));

    }

    clock_nanosleep(CLOCK_MONOTONIC, 0, &sleep_t, NULL);

    if (exit_counter>=2)
      break;
  }

  pthread_exit(0);

}


void init_module () {

  pthread_mutex_init(&mutex, NULL);
  pthread_cond_init(&is_full, NULL);

  buffer_counter=0;

  memset(&thread_processing, 0, sizeof (pthread_t));
  pthread_create(&thread_processing, NULL, (void *)process_commands, NULL);

}

void finish_module() {

  pthread_join(thread_processing,NULL);

  pthread_cond_destroy(&is_full);
  pthread_mutex_destroy(&mutex);


}

void print_commands () {


  for (int i=0; i<buffer_counter; i++)
    printf("%c\n", buffer[i]);


}

void fill_buffer(char c){

  my_sleep();

  while (buffer_counter==MAX_BUFFER){
    printf("Buffer lleno, esperando ...\n");
    pthread_cond_wait(&is_full, &mutex);
  }

  printf("Añadiendo comando al buffer\n");

  buffer[buffer_counter]=c;
  buffer_counter++;

}


// Al implementar este modulo monitores, todas las funciones deben asegurar
// exclusion mutua para evitar que varios threads ejecuten diferentes
// funciones a la vez.

void move_robot_right() {

  pthread_mutex_lock(&mutex);
  fill_buffer('R');
  pthread_mutex_unlock(&mutex);

  pthread_exit(0);
}

void move_robot_left() {

  pthread_mutex_lock(&mutex);
  fill_buffer('L');
  pthread_mutex_unlock(&mutex);

  pthread_exit(0);
}
void move_robot_forward() {

  pthread_mutex_lock(&mutex);
  fill_buffer('F');
  pthread_mutex_unlock(&mutex);

  pthread_exit(0);

}
void move_robot_backward() {

  pthread_mutex_lock(&mutex);
  fill_buffer('B');
  pthread_mutex_unlock(&mutex);


  pthread_exit(0);
}


