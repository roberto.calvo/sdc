/*
* Copyright (C) 2020 by Roberto Calvo-Palomino
*
*
*  This programa is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with RTL-Spec.  If not, see <http://www.gnu.org/licenses/>.
*
* 	Authors: Roberto Calvo-Palomino <roberto.calvo [at] urjc [dot] es>
*/

// Asignatura: Sistemas Distribuidos y Concurrentes
// Universidad Rey Juan Carlos

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include "monitor.h"

#define MAX_THREADS 100

int main(void) {
  time_t t;
  unsigned int rand_function;

  // Definimos un array de las funciones de movimiento del robot
  void (*funcs[4]) () = {move_robot_right,
                        move_robot_left,
                        move_robot_forward,
                        move_robot_backward};

  // Definición e inicialización del poll the threads.
  pthread_t thread_poll[MAX_THREADS];
  memset (thread_poll, 0, MAX_THREADS * sizeof (pthread_t));

  // Inicializamos el generador de numeros aleatorios
  srand((unsigned) time(&t));

  // Inicializamos el modulo del robot.
  init_module();

  // Generamos MAX_THREADS con funciones aleatorias de movimientos del robot.
  for (int i=0; i<MAX_THREADS; i++) {
    rand_function=rand() % 4;
    pthread_create(&thread_poll[i], NULL, (void *)funcs[rand_function], NULL);
  }

  for (int i=0; i<MAX_THREADS; i++) {
    pthread_join(thread_poll[i],NULL);
  }

  //print_commands();

  finish_module();
  return 0;

}

